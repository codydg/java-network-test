package networktest;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class Client implements Task {
    private DatagramSocket m_socket;

    public Client() {
        System.out.println("Constructing Client");
    }

    @Override
    public void close() {
        System.out.println("Closing Client");
        m_socket.close();
    }

    @Override
    public void run() {
        System.out.println("Starting Client");

        InetAddress serverAddr;
        try {
            serverAddr = InetAddress.getByName("192.168.1.138");
        } catch (UnknownHostException e) {
            System.err.println("Failed to start client!");
            return;
        }

        try {
            m_socket = new DatagramSocket();
        } catch (SocketException e) {
            System.err.println("Failed to start client!");
            return;
        }

        System.out.println("Client Started!");
        while (!m_socket.isClosed()) {
            //byte[] bytes = "Hello World!".getBytes();
            ByteBuffer b = ByteBuffer.allocate(65000);
            var bytes = b.array();
            DatagramPacket p = new DatagramPacket(bytes, bytes.length, serverAddr, 3000);
            try {
                System.out.println("getReceiveBufferSize: " + m_socket.getReceiveBufferSize());
                System.out.println("getSendBufferSize " + m_socket.getSendBufferSize());
                m_socket.send(p);
            } catch (IOException e) {
                System.out.println("Warning: Failed to send packet!");
                continue;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                // Do Nothing
            }
        }
    }
}
