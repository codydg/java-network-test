package networktest;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

public class Server implements Task {
    private DatagramSocket m_socket;

    public Server() {
        System.out.println("Constructing Server");
    }

    @Override
    public void close() {
        System.out.println("Closing Server");
        m_socket.close();
    }

    @Override
    public void run() {
        System.out.println("Starting Server");

        try {
            m_socket = new DatagramSocket(3000);
        } catch (SocketException e) {
            System.err.println("Failed to start server!");
            return;
        }

        System.out.println("Server Started!");
        while (!m_socket.isClosed()) {
            var bytes = new byte[70000];
            var packet = new DatagramPacket(bytes, bytes.length);
            try {
                m_socket.receive(packet);
            } catch (SocketException e) {
                // Socket Closed?
                continue;
            } catch (IOException e) {
                System.out.println("Warning: Failed to read packet!");
                continue;
            }
            var message = new String(packet.getData(), 0, packet.getLength());
            System.out.println(message.length());
        }
        m_socket.close();
    }
}
