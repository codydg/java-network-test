package networktest;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) {
        List<Task> tasks = new ArrayList<>();
        for (String arg : args) {
            switch (arg) {
                case "--client":
                    tasks.add(new Client());
                    break;
                case "--server":
                    tasks.add(new Server());
                    break;
                default:
                    System.err.println("Unknown Arg: " + arg);
            }
        }

        List<Thread> threads = tasks.stream().map(Thread::new).collect(Collectors.toList());
        threads.forEach(Thread::start);

        System.out.println("Q to quit");
        Scanner s = new Scanner(System.in);
        while (!s.nextLine().toLowerCase().equals("q"));
        s.close();

        tasks.forEach(Task::close);
        for (var t : threads) {
            try {
                t.join();
            } catch (InterruptedException e) {
                System.err.println("Failed to join a thread!");
            }
        }
    }
}
