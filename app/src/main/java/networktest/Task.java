package networktest;

public interface Task extends Runnable {
    void close();
}
